" default directory
if has('nvim')
    let g:default_agenda_dir = get(g:, 'default_agenda_dir', "~/.config/nvim/vim-agenda/")
    let g:expanded_default_agenda_dir = expand(g:default_agenda_dir)
else
    let g:default_agenda_dir = get(g:, 'default_agenda_dir', "~/.vim/vim-agenda/")
    let g:expanded_default_agenda_dir = expand(g:default_agenda_dir)
endif
let g:default_agenda_file = get(g:, 'default_agenda_file', "agenda.md")

" TODO: add an 'ALL' option
let g:TODO_DICT = {
    \ 1: 'TODO',
    \ 2: 'DONE',
    \ 3: 'TODO\|DONE\|NOTE\|OPTIMIZE\|FIXME'
    \ }

let g:COMMENT_TYPE = {
    \ 'md': '#'
  \ }

autocmd BufWritePost * call sammys_vim_agenda#CongregateAgenda(3)
autocmd VimEnter * call sammys_vim_agenda#CongregateAgenda(3)
autocmd QuitPre * call sammys_vim_agenda#CheckBuffer()
command! -nargs=0 OpenAgenda call sammys_vim_agenda#OpenAgenda()
command! -nargs=0 CreateProject call sammys_vim_agenda#CreateAgendaProject()
command! -nargs=0 DeleteProject call sammys_vim_agenda#DeleteProject()
command! -nargs=0 EditProject call sammys_vim_agenda#EditProject()
command! -nargs=0 AddSubModule call sammys_vim_agenda#AddSubModule()
command! -nargs=0 AddToProject call sammys_vim_agenda#AddToProject()
