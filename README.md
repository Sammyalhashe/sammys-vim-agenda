# Example Usage
- Create/Edit projects/subprojects
- Maintain a list of tasks in each project
- Pull up scratch buffers that contain a list of your tasks for specific projects (or all projects)
- Keybindings (sd, sf, so, st) used to toggle heading between DONE, FIXME, OPTIMIZE, and TODO
- Syntax highlighting on keywords TODO, FIXME, OPTIMIZE, and DONE in these scratch buffers
![](img/example.gif?raw=true)
