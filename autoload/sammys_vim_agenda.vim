" Function I found online, highly unecessary I can just do this with delete()
function! sammys_vim_agenda#DeleteFile(...)
  if(exists('a:1'))
    let theFile=a:1
  elseif ( &ft == 'help' )
    echohl Error
    silent echo "Cannot delete a help buffer!"
    echohl None
    return -1
  else
    let theFile=expand('%:p')
  endif
  let delStatus=delete(theFile)
  if(delStatus == 0)
    silent echo "Deleted " . theFile
  else
    echohl WarningMsg
    echo "Failed to delete " . theFile
    echohl None
  endif
  return delStatus
endfunction

" Creates a directory if it doesn't already exist
function! sammys_vim_agenda#CreateDirectoryIfNotExists(fp) abort
    if (!isdirectory(a:fp) && exists('*mkdir'))
        call mkdir(a:fp, "p") " default protection bits 0755
        echohl Identifier
        silent echo "\n" . a:fp . " created successfully"
    elseif !isdirectory(a:fp)
        echohl WarningMsg
        echo "\nUnable to create directory"
    endif
endfunction

" Creates a file if it doesn't already exist
function! sammys_vim_agenda#CreateFileIfNotExists(fp) abort
    if empty(glob(a:fp))
        " TODO: Add compatibility with DOS
        call system("touch " . a:fp)
        " NOTE: I like the above better than the above
        " execute '!touch ' . a:fp
        return
    endif
    echo "Not Created"
endfunction

function! sammys_vim_agenda#DeleteDirectoryContents(fp) abort
    let l:dirContents = split(globpath(a:fp, '*'), '\n')
    for dirContent in l:dirContents
        if isdirectory(dirContent)
            " Delete directory contents before the directory itself
            " Otherwise delete will not work
            call sammys_vim_agenda#DeleteDirectoryContents(dirContent)
            call delete(dirContent, "d")
        else
            call delete(dirContent)
        endif
    endfor
endfunction

" Deletes a directory if it exists
function! sammys_vim_agenda#DeleteDirectoryIfExists(fp) abort
    if (isdirectory(a:fp))
        call delete(a:fp, "rf") " NOTE: deletes recursively
        echohl Identifier
        echo "\nSuccessfully deleted directory " . a:fp
    else
        echohl WarningMsg
        echo "\nUnable to delete directory " . a:fp
    endif
endfunction

" create the agenda folder if it doesn't exist
call sammys_vim_agenda#CreateDirectoryIfNotExists(g:expanded_default_agenda_dir)

" Scan over the directory and recursively obtain filepaths
function! sammys_vim_agenda#GetAgendaFilesAt(startingDir) abort
    let l:dirContents = split(globpath(a:startingDir, '*'), '\n')
    let l:files = []
    if len(l:dirContents) > 0
        " check if a file is
        for file in l:dirContents
            if isdirectory(file)
                " recursively scan over the subdirectory
                let l:files = l:files + sammys_vim_agenda#GetAgendaFilesAt(l:file)
            else
                " add the file
                if (glob(file) !~ g:expanded_default_agenda_dir . g:default_agenda_file)
                    let l:files = add(l:files, file)
                endif
            endif
        endfor
    endif
    return l:files
endfunction

" Scan over the directories and find all "Projects"
function! sammys_vim_agenda#GetProjectPaths(startingDir) abort
    let l:dirContents = split(globpath(a:startingDir, '*'), '\n')
    let l:dirs = []
    if len(l:dirContents) > 0
        for file in l:dirContents
            if isdirectory(file)
                " Add the current path
                let l:dirs = add(l:dirs, file)
                " Recursively find other options
                let l:dirs = l:dirs + sammys_vim_agenda#GetProjectPaths(file)
            endif
        endfor
    endif
    return l:dirs
endfunction

" get all the agenda files from the default directory
function! sammys_vim_agenda#GetAgendaFiles() abort
    return sammys_vim_agenda#GetAgendaFilesAt(g:default_agenda_dir)
endfunction

" scans the file for the requested keywords and returns
" an array of the line and the line number found
function! sammys_vim_agenda#ScanFile(file, searchType) abort
    let l:lines = readfile(a:file)
    let l:filteredLines = []
    let l:lineCount = 1
    for line in l:lines
        " if the line contains a todo
        " DONE: change it such that it should be at the beginning of the line
        " if tolower(line) =~ b:TODO_DICT[a:searchType]
        let l:tmpMatch = matchstr(line, '^\s*[-\|*]*\s*\(' . g:TODO_DICT[a:searchType] . '\)')
        if !empty(l:tmpMatch)
            let matchIdx = match(line, '\(' . g:TODO_DICT[a:searchType] . '\)')
            let l:filteredLines = add(l:filteredLines, [line[matchIdx:], l:lineCount])
        endif
        " increment the line number that we are examining
        let l:lineCount = l:lineCount + 1
    endfor
    return l:filteredLines
endfunction

function! sammys_vim_agenda#ScanAgendaFiles(agendaFiles, type) abort
    let l:filteredFiles = {}
    for file in a:agendaFiles
        " scan the files for text
        " let the lines be indexed by the file
        let l:tmp = sammys_vim_agenda#ScanFile(file, a:type)
        if len(l:tmp) != 0
            let l:filteredFiles[file] = l:tmp
        endif
    endfor
    return l:filteredFiles
endfunction

function! sammys_vim_agenda#ReadAgenda(type) abort
    let l:files = sammys_vim_agenda#GetAgendaFiles()
    return sammys_vim_agenda#ScanAgendaFiles(l:files, a:type)
endfunction

function! sammys_vim_agenda#AppendProjectToFile(file, projectName, tasks) abort
    " add the comment that designates a specific project
    call writefile([g:COMMENT_TYPE['md'] . " " . a:projectName], a:file, "a")
    let l:tmpLines = []
    for l:task in a:tasks
        let l:taskText = l:task[0]
        let l:lineNbr = l:task[1]
        let l:tmpLines = add(l:tmpLines, l:taskText)
    endfor
    call writefile(l:tmpLines, a:file, "a")
    " add space between projects -> a flag for append
    call writefile(["", "", ""], a:file, "a")
endfunction

" congregates requested agenda type in a buffer (or file?)
" TODO: Figure out how to update the buffer if it is already open (maybe edit!)
function! sammys_vim_agenda#CongregateAgenda(type) abort
    let l:currBuffer = bufnr('%') " NOTE: '%' means current buffer
    let l:lines = sammys_vim_agenda#ReadAgenda(a:type)
    let l:file = g:expanded_default_agenda_dir . g:default_agenda_file
    if (filereadable(l:file))
        silent echo "delete " . l:file
        call sammys_vim_agenda#DeleteFile(l:file)
        call writefile([""], l:file) " NOTE: May be able to replace this with CreateFileIfNotExists()
    endif
    for item in items(l:lines)
        let l:subs = substitute(item[0], g:expanded_default_agenda_dir, "", "")
        call sammys_vim_agenda#AppendProjectToFile(l:file, l:subs, item[1])
    endfor
    if bufname(l:currBuffer) == g:expanded_default_agenda_dir . g:default_agenda_file
        checktime
    endif
endfunction

" function to open a project file in a new buffer
function! sammys_vim_agenda#OpenFile(fp) abort
    execute "rightbelow vsplit " . a:fp
    setl filetype=sva
    setl bufhidden=hide
    setl autoread
    nnoremap <silent><buffer> sd :call sammys_vim_agenda#FindFirstTODO(line("."), 3, "DONE", v:true)<cr>
    nnoremap <silent><buffer> st :call sammys_vim_agenda#FindFirstTODO(line("."), 3, "TODO", v:true)<cr>
    nnoremap <silent><buffer> sf :call sammys_vim_agenda#FindFirstTODO(line("."), 3, "FIXME", v:true)<cr>
    nnoremap <silent><buffer> so :call sammys_vim_agenda#FindFirstTODO(line("."), 3, "OPTIMIZE", v:true)<cr>
endfunction

function! sammys_vim_agenda#AgendaBufferNum() abort
    let l:all = range(0, bufnr('$')) "'$' means the last buffer
    for b in l:all
        if bufname(b) == g:expanded_default_agenda_dir . g:default_agenda_file
            " echo bufname(b) TODO: this needs validation
            return b
        endif
    endfor
    return -1
endfunction

function! sammys_vim_agenda#FindFirstTODO(linenr, searchType, changeTo, save) abort
  " go to the front of the line
  " call execute("normal 0")
  " get the content of a line
  let l:currentLineContent = getline(a:linenr)
  " length of the current line
  let l:lenLine = len(l:currentLineContent)
  " column number of the first match of a 'TODO'
  let l:matchColNr = match(l:currentLineContent, g:TODO_DICT[a:searchType])
  " check if the value > - 1
  if l:matchColNr > -1
    " call substitute on the string to replace the first occurence
    let l:newLineContent = substitute(l:currentLineContent, g:TODO_DICT[a:searchType], a:changeTo, "")
    call setline(a:linenr, l:newLineContent)
  endif
  if a:save
    silent! execute "w"
  endif
endfunction

function! CheckFirstCharacterInLine(linenr) abort
endfunction

function! GoBackUntilFilePath(linenr) abort
  let l:currLineNr = a:linenr
  while l:currLineNr >= 0
    let l:currLineContent = getline(l:currLineNr)
    " replace all leading whitespace with nothing
    let l:strippedContent = substitute(l:currLineContent, "^\\s*", "", "g")
    let l:firstChar = l:strippedContent[0]
    " case insensitive no matter what the user has set comparison
    if l:firstChar ==? "#"
      return [substitute(substitute(l:strippedContent, "#", "", ""), "^\\s*", "", "g"), l:currLineNr]
    endif
    let l:currLineNr = l:currLineNr - 1
  endwhile
  return ["", -1]
endfunction

function! CountLinesOfBlock(linenr) abort
  let l:currLine = a:linenr
  let l:blocksize = 0
  " case insensitive
  while getline(l:currLine) !=? ""
    let l:blocksize = l:blocksize + 1
    let l:currLine = l:currLine + 1
  endwhile
  return l:blocksize
endfunction


function! Switch(linenr, change) abort
  call sammys_vim_agenda#FindFirstTODO(a:linenr, 3, a:change, v:false)
  let l:filePathDetails = GoBackUntilFilePath(a:linenr)
  if l:filePathDetails[1] != -1
    " write to the file that was changed
    let l:lines = [] 
    " plus 1 because the first line is the comment we don't care about
    let l:blockSize = CountLinesOfBlock(l:filePathDetails[1] + 1)
    let l:currLine = l:filePathDetails[1] + 1
    let l:idx = 0
    while l:idx < l:blockSize
      let l:currLineContent = getline(l:currLine)
      let l:lines = add(l:lines, l:currLineContent)
      let l:idx = l:idx + 1
      let l:currLine = l:currLine + 1
    endwhile
    " disable autocmd for this one thing
    setl autoread
    call writefile(l:lines, g:expanded_default_agenda_dir . l:filePathDetails[0], "")
  endif
endfunction

" function to open the agenda file in a new buffer
function! sammys_vim_agenda#OpenAgenda() abort
    if bufname("%") != g:expanded_default_agenda_dir . g:default_agenda_file
        let l:bIdx = sammys_vim_agenda#AgendaBufferNum()
        if l:bIdx == -1 " || == 0
            execute "rightbelow vsplit! " . g:expanded_default_agenda_dir . g:default_agenda_file
        else
            execute "rightbelow vsplit! " . g:expanded_default_agenda_dir . g:default_agenda_file
            let fname = bufname(l:bIdx)
            edit! "" . fname
        endif
        setl filetype=sva
        setl buftype=nofile
        setl bufhidden=hide
        setl autoread
        nnoremap <silent><buffer> sd :call Switch(line("."), "DONE")<cr>
        nnoremap <silent><buffer> st :call Switch(line("."), "TODO")<cr>
        nnoremap <silent><buffer> sf :call Switch(line("."), "FIXME")<cr>
        nnoremap <silent><buffer> so :call Switch(line("."), "OPTIMIZE")<cr>
    endif
endfunction

" Creates a Project Directory
function! sammys_vim_agenda#CreateAgendaProject() abort
    call inputsave()
    let l:fName = input("Enter the project name: ")
    call inputrestore()
    let l:expandedName = g:expanded_default_agenda_dir . l:fName
    call sammys_vim_agenda#CreateDirectoryIfNotExists(l:expandedName)
endfunction

" DONE: function to split by '/' and add a '&' to the last word
" TODO: May have to modify this so that it supports more projects
" NOTE: Thinking about finding a way to add extra characters in front
"       Ie. (a)Sammy (b)Test ... (aa)asdf (ab)qwer
function sammys_vim_agenda#SplitAndAddCharToLast(toSplit, separator, charToAdd, chosen) abort
    let l:haveSplit = split(a:toSplit, a:separator)
    let l:lchosen = l:haveSplit[-1][0]
    let l:idx = 0
    while match(l:lchosen, "[&\|,\|/\|\]") != -1 || (l:idx + 1 < len(l:haveSplit[-1]) && index(a:chosen, l:lchosen) >= 0)
        let l:idx = l:idx + 1
        let l:lchosen = l:haveSplit[-1][l:idx]
    endwhile
    call add(a:chosen, l:lchosen) " add the chosen char

    let l:haveSplit[-1] = (idx == 0 ? '' : l:haveSplit[-1][: idx - 1]) . "&" . l:haveSplit[-1][idx:]
    return join(l:haveSplit, "/")
endfunction

" Adds Subproject to Project
function! sammys_vim_agenda#AddSubModule() abort
    let l:lettersChosen = []
    " DONE: grab the projects and separate them by newLines
    let l:projectPaths = sammys_vim_agenda#GetProjectPaths(g:expanded_default_agenda_dir)
    let l:copy = copy(l:projectPaths)
    let l:subs = map(l:projectPaths, 'substitute(v:val, g:expanded_default_agenda_dir, "", "")')
    " NOTE: add Cancel option
    call add(l:subs, "Cancel")
    " TODO: add '&' to word after the last '/'
    let l:subs = map(l:subs, {_, val -> sammys_vim_agenda#SplitAndAddCharToLast(val, "/", "&", l:lettersChosen)})
    let l:shortenedProjectPaths = join(l:subs, "\n")
    let l:projectIdx = confirm("Enter the project to edit: ", l:shortenedProjectPaths, "Question")
    if l:projectIdx == len(l:subs)
        echohl WarningMsg
        echo "Cancelled"
        return
    endif
    let l:projectPathChosen = l:copy[l:projectIdx - 1]
    call inputsave()
    let l:subModuleName = input("Enter submodule name: ")
    call inputrestore()
    call sammys_vim_agenda#CreateDirectoryIfNotExists(l:projectPathChosen . "/" . l:subModuleName)
endfunction

" function adds new files to the project
function! sammys_vim_agenda#AddToProject() abort
    let l:lettersChosen = []
    " DONE: grab the projects and separate them by newLines
    let l:projectPaths = sammys_vim_agenda#GetProjectPaths(g:expanded_default_agenda_dir)
    let l:copy = copy(l:projectPaths)
    let l:subs = map(l:projectPaths, 'substitute(v:val, g:expanded_default_agenda_dir, "", "")')
    " NOTE: add Cancel option
    call add(l:subs, "Cancel")
    " TODO: add '&' to word after the last '/'
    let l:subs = map(l:subs, {_, val -> sammys_vim_agenda#SplitAndAddCharToLast(val, "/", "&", l:lettersChosen)})
    let l:shortenedProjectPaths = join(l:subs, "\n")
    let l:projectIdx = confirm("Enter the project to edit: ", l:shortenedProjectPaths, "Question")
    if l:projectIdx == len(l:subs)
        echohl WarningMsg
        echo "Cancelled"
        return
    endif
    let l:projectPathChosen = l:copy[l:projectIdx - 1]
    call inputsave()
    let l:fileName = input("Enter file name: ")
    call inputrestore()
    call sammys_vim_agenda#CreateFileIfNotExists(l:projectPathChosen . "/" . l:fileName)
endfunction

function! sammys_vim_agenda#EditProject() abort
    let l:lettersChosen = []
    " DONE: grab the projects and separate them by newLines
    let l:projectPaths = sammys_vim_agenda#GetProjectPaths(g:expanded_default_agenda_dir)
    let l:copy = copy(l:projectPaths)
    let l:subs = map(l:projectPaths, 'substitute(v:val, g:expanded_default_agenda_dir, "", "")')
    " NOTE: add Cancel option
    call add(l:subs, "Cancel")
    " TODO: add '&' to word after the last '/'
    let l:subs = map(l:subs, {_, val -> sammys_vim_agenda#SplitAndAddCharToLast(val, "/", "&", l:lettersChosen)})
    let l:shortenedProjectPaths = join(l:subs, "\n")
    let l:projectIdx = confirm("Enter the project to edit: ", l:shortenedProjectPaths, "Question")
    if l:projectIdx == len(l:subs)
        echohl WarningMsg
        echo "Cancelled"
        return
    endif
    let l:projectPathChosen = l:copy[l:projectIdx - 1]
    let l:projectFiles = sammys_vim_agenda#GetAgendaFilesAt(l:projectPathChosen)
    if len(l:projectFiles) == 0
        call inputsave()
        if confirm("This project has no files, would you like to create one? ", "&Yes\n&No", "Question") == 1
            call inputsave()
            let l:fileName = input("Enter file name: ")
            call inputrestore()
            call sammys_vim_agenda#CreateFileIfNotExists(l:projectPathChosen . "/" . l:fileName)
            call sammys_vim_agenda#OpenFile(l:projectPathChosen . "/" . l:fileName)
            return
        endif
        call inputrestore()
    endif
    let l:copy2 = copy(l:projectFiles)
    let l:fileNames = map(l:projectFiles, 'substitute(v:val, g:expanded_default_agenda_dir, "", "")')
    " NOTE: add Cancel option
    call add(l:fileNames, "Cancel")
    let l:lettersChosen = []
    let l:fileNames = map(l:fileNames, {_, val -> sammys_vim_agenda#SplitAndAddCharToLast(val, "/", "&", l:lettersChosen)})
    let l:shortenedFilePaths = join(l:fileNames, "\n")
    let l:fileIdx = confirm("Enter the file to edit: ", l:shortenedFilePaths, "Question")
    if l:fileIdx == len(l:fileNames)
        echohl WarningMsg
        echo "Cancelled"
        return
    endif
    let l:filePathChosen = l:copy2[l:fileIdx - 1]
    call sammys_vim_agenda#OpenFile(l:filePathChosen)
endfunction

" Delete project from Agenda and ask for confirmation
function! sammys_vim_agenda#DeleteProject() abort
    let l:lettersChosen = []
    " DONE: grab the projects and separate them by newLines
    let l:projectPaths = sammys_vim_agenda#GetProjectPaths(g:expanded_default_agenda_dir)
    let l:copy = copy(l:projectPaths)
    let l:subs = map(l:projectPaths, 'substitute(v:val, g:expanded_default_agenda_dir, "", "")')
    " NOTE: add Cancel option
    call add(l:subs, "Cancel")
    " TODO: add '&' to word after the last '/'
    let l:subs = map(l:subs, {_, val -> sammys_vim_agenda#SplitAndAddCharToLast(val, "/", "&", l:lettersChosen)})
    let l:shortenedProjectPaths = join(l:subs, "\n")
    let l:projectIdx = confirm("Enter the project to delete: ", l:shortenedProjectPaths, "Question")

    if l:projectIdx == len(l:subs)
        echohl WarningMsg
        echo "Cancelled"
        return
    endif

    let l:projectPathChosen = l:copy[l:projectIdx - 1]
    let l:idx = confirm("Are you sure?: ", "&Yes\n&No", "Question")
    if l:idx == 1
        call sammys_vim_agenda#DeleteDirectoryIfExists(l:projectPathChosen)
    elseif l:idx == 2
        echohl WarningMsg
        echo "Cancelled"
    endif
endfunction

function! sammys_vim_agenda#CheckBuffer() abort
    let l:currBuffer = bufnr('%') " NOTE: '%' means current buffer
    " if it's the default agenda file or another one of the deriving agenda files
    if bufname(l:currBuffer) == g:expanded_default_agenda_dir . g:default_agenda_file || match(bufname(l:currBuffer), "^\\s*" . g:expanded_default_agenda_dir) > -1
        bdelete
    endif
endfunction
