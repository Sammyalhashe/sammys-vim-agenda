if exists("b:current_syntax")
  finish
endif

syntax keyword svaTODO TODO TODO:
syntax keyword svaDONE DONE DONE:
syntax keyword svaOPTIMIZE OPTIMIZE OPTIMIZE:
syntax keyword svaFIXME FIXME FIXME:
syntax keyword svaWAIT WAIT WAIT:
syntax keyword svaLIST - + *

highlight link svaTODO Keyword
highlight link svaDONE Comment
highlight link svaOPTIMIZE Tag
highlight link svaFIXME Exception
highlight link svaWAIT Number
highlight link svaLIST SpecialChar

let b:current_syntax = "sva"
